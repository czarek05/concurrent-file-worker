import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicReference;

public class ConcurrentFileWorker {

    public static void main(String[] args) {

        if (args.length < 2 || args.length > 5) {
            System.out.println("Number of passed arguments is invalid");
            System.exit(1);
        }

        String sourcePathString = args[0];
        String destPathString = args[1].endsWith("\\") ? args[1] : args[1] + "\\";
        Path sourcePath = Paths.get(sourcePathString);
        Path destPath = Paths.get(destPathString);
        if (!Files.isDirectory(sourcePath) || !Files.isDirectory(destPath)) {
            System.out.println("Provided path do not exist");
            System.exit(1);
        }

        long timeForCopy = 10000;
        long waitBetweenThreadAlivenessCheck = 10000;
        long waitAfterThreadDead = 10000;
        try {
            if (args.length == 3) {
                timeForCopy = Long.parseLong(args[2]);
            } else if (args.length == 4) {
                timeForCopy = Long.parseLong(args[2]);
                waitBetweenThreadAlivenessCheck = Long.parseLong(args[3]);
            } else if (args.length == 5) {
                timeForCopy = Long.parseLong(args[2]);
                waitBetweenThreadAlivenessCheck = Long.parseLong(args[3]);
                waitAfterThreadDead = Long.parseLong(args[4]);
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid duration-related argument");
            System.exit(1);
        }


        // destPath directory contains files that were copied
        File destFile = destPath.toFile();
        Vector<String> alreadyProcessedFiles = new Vector<>();
        for (File f : destFile.listFiles()) {
            alreadyProcessedFiles.add(f.getName());
        }


        // the purpose of filesPaths and filesNames is to store info about files
        // not processed yet = files which should be copied
        Path path = Paths.get(sourcePathString);
        File file = path.toFile();
        Vector<String> filesPaths = new Vector<>();
        Vector<String> filesNames = new Vector<>();
        File[] listFilesSimpleArray = file.listFiles();
        List<File> listFiles = new ArrayList<>();
        for (File f : listFilesSimpleArray) {
            listFiles.add(f);
        }
        Collections.shuffle(listFiles);
        for (File f : listFiles) {
            if (!alreadyProcessedFiles.contains(f.getName())) {
                filesPaths.add(f.getAbsolutePath());
                filesNames.add(f.getName());
            }
        }
        if (filesPaths.isEmpty()) {
            System.exit(0);
        }

        AtomicReference<Integer> filesIndex = new AtomicReference<>(0);

        AtomicReference<String> fileProcessedPath = new AtomicReference<>(filesPaths.get(filesIndex.get()));
        AtomicReference<String> filesDestinationDir = new AtomicReference<>(destPathString);
        AtomicReference<String> currentFileDestination = new AtomicReference<>(filesDestinationDir.get() + filesNames.get(filesIndex.get()));

        // runnable, which is responsible for copying a files; if still there are files to copy,
        // change ""pointers"" to next file (fileProcessedPath) and next file destination (currentFileDestination)
        Runnable fileProcessor = () -> {
            while (true) {
                try {
                    System.out.println("I am processing file: " + fileProcessedPath.get());
                    Path originalPath = Paths.get(fileProcessedPath.get());
                    System.out.println("I have created corresponding Path object");
                    Path copied = Paths.get(currentFileDestination.get());
                    System.out.println("I have created Path object, which represents copy destination");
                    Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("I have copied the file");
                    if (filesIndex.get() < filesPaths.size() - 1) {
                        filesIndex.set(filesIndex.get() + 1);
                        fileProcessedPath.set(filesPaths.get(filesIndex.get()));
                        currentFileDestination.set(filesDestinationDir.get() + filesNames.get(filesIndex.get()));
                    } else {
                        System.exit(0);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        // tempThread for fileProcessor runnables
        AtomicReference<Thread> tempThread = new AtomicReference<>(new Thread(fileProcessor));


        // the idea behind the concurrencyManager runnable is to monitor whether operation of copying a file
        // is taking more than 10 seconds by comparing if file being processed and the file that had been processed
        // 10 seconds before are the same; if yes, the operation should be stopped (by suspending file processor
        // thread), then there is something similar like in the fileProcessor (if condition is fulfilled,
        // move to the next file) and finally the new fileProcessor thread is created
        long finalTimeForCopy = timeForCopy;
        long finalWaitAfterThreadDead = waitAfterThreadDead;
        long finalWaitBetweenThreadAlivenessCheck = waitBetweenThreadAlivenessCheck;
        Runnable concurrencyManager = () -> {
            while (true) {
                try {
                    System.out.println("I am in concurrencyManager");
                    AtomicReference<String> lastProcessedFile = new AtomicReference<>(fileProcessedPath.get());
                    Thread.sleep(finalTimeForCopy);
                    if (lastProcessedFile.get().equals(fileProcessedPath.get())) {
                        System.out.println("I am stopping fileProcessor");
                        tempThread.get().stop();

                        if (filesIndex.get() < filesPaths.size() - 1) {
                            filesIndex.set(filesIndex.get() + 1);
                            fileProcessedPath.set(filesPaths.get(filesIndex.get()));
                            currentFileDestination.set(filesDestinationDir.get() + filesNames.get(filesIndex.get()));
                        } else {
                            System.exit(0);
                        }
                        while (true) {
                            if (!tempThread.get().isAlive()) {
                                Thread.sleep(finalWaitAfterThreadDead);
                                break;
                            }
                            Thread.sleep(finalWaitBetweenThreadAlivenessCheck);
                        }
                        System.out.println("I am creating new fileProcessor");
                        tempThread.set(new Thread(fileProcessor));
                        tempThread.get().start();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread concurrencyManagerThread = new Thread(concurrencyManager);


        tempThread.get().start();
        concurrencyManagerThread.start();
    }

}
