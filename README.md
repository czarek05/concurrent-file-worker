# Concurrent File Worker
Copy files. Stop and go to the next file, if the process takes too long.

Concurrent File Worker copies all files from one chosen directory to another chosen directory. If the app can not copy a file in a specific duration, it stops coping that file and try to copy next file. There was a need to not use file size checking before starting the copy process. App uses deprecated methods.

## Usage
Compile:

`javac ConcurrentFileWorker.java`

Run:

`java ConcurrentFileWorker SOURCEDIR DESTDIR [TIMEFORCOPY] [WAITBETWEENTHREADALIVENESSCHECK] [WAITAFTERTHREADDEAD]`

TIMEFORCOPY - time (in milliseconds) allowed for copying a single file, when passes, copy thread is stopped (default: 10 seconds)

WAITBETWEENTHREADALIVENESSCHECK - time (in milliseconds) to wait after each thread aliveness check (default: 10 seconds)

WAITAFTERTHREADDEAD - time (in milliseconds) to wait after noticing that copy thread died, time to wait before creating new copy thread (default: 10 seconds)
